package io.niceday

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.niceday.button.ButtonActivity
import io.niceday.button.CheckBoxActivity
import io.niceday.button.RadioActivity
import io.niceday.legacy.ListViewActivity
import io.niceday.text.TextEditActivity
import io.niceday.widget.ImageViewActivity
import io.niceday.widget.ProgressBarActivity
import io.niceday.widget.SeekBarActivity
//import io.niceday.button.RadioButtonActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnButton.setOnClickListener {
            startActivity(Intent(this, ButtonActivity::class.java))
        }

        btnCheckBox.setOnClickListener {
            startActivity(Intent(this, CheckBoxActivity::class.java))
        }

        btnRadio.setOnClickListener {
            startActivity(Intent(this, RadioActivity::class.java))
        }

        btnProgress.setOnClickListener {
            startActivity(Intent(this, ProgressBarActivity::class.java))
        }

        btnSeekBar.setOnClickListener {
            startActivity(Intent(this, SeekBarActivity::class.java))
        }

        btnTextEdit.setOnClickListener {
            startActivity(Intent(this, TextEditActivity::class.java))
        }

        btnImageView.setOnClickListener {
            startActivity(Intent(this, ImageViewActivity::class.java))
        }

        btnListView.setOnClickListener {
            startActivity(Intent(this, ListViewActivity::class.java))
        }

        btnSpinner.setOnClickListener {
            startActivity(Intent(this, SpinnerActivity::class.java))
        }

    }

}
