package io.niceday.legacy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import io.niceday.R
import kotlinx.android.synthetic.main.activity_list_view.*

class ListViewActivity : AppCompatActivity() {

    private var items = arrayListOf<String>("아이템 1", "아이템 2")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)

        var adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        listView.adapter = adapter

        button1.setOnClickListener {
            items.add("아이템 ${items.size + 1}")
            adapter.notifyDataSetChanged();
        }

        button2.setOnClickListener {
            items.clear()
            adapter.clear()
        }
        
        listView.setOnItemClickListener{ parent, view, position, id ->
            textView.text = items[position]
            adapter.notifyDataSetChanged()
        }
    }
}
