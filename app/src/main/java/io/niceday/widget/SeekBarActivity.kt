package io.niceday.widget

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import io.niceday.R
import kotlinx.android.synthetic.main.activity_seek_bar.*

class SeekBarActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seek_bar)

        seekBar1.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                textView1.text = progress.toString();
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // 값 변경을 시작 할때
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // 값 변경을 종료 할때
            }
        })

        seekBar2.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                textView2.text = progress.toString();
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // 값 변경을 시작 할때
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // 값 변경을 종료 할때
            }
        })
    }
}
