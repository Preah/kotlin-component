package io.niceday.widget

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.niceday.R
import kotlinx.android.synthetic.main.activity_progress_bar.*

class ProgressBarActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progress_bar)


        button1.setOnClickListener {
            progressBar2.incrementProgressBy(5)
            textView.text = progressBar2.progress.toString()
        }

        button2.setOnClickListener {
            progressBar2.incrementProgressBy(-5)
            textView.text = progressBar2.progress.toString()
        }

    }
}
