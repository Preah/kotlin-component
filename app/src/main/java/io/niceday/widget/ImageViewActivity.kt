package io.niceday.widget

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.niceday.R

class ImageViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)
    }
}
