package io.niceday.text

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.widget.EditText
import android.widget.TextView
import io.niceday.R
import kotlinx.android.synthetic.main.activity_text_edit.*

class TextEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text_edit)

        editText1.setOnEditorActionListener(EnterListener())
        editText2.setOnEditorActionListener(EnterListener())
        editText3.setOnEditorActionListener(EnterListener())
    }

    inner class EnterListener : TextView.OnEditorActionListener {
        override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
            when((v as EditText).id) {
                editText1.id -> textView.text = "Edit 1 : ${v.text}"
                editText2.id -> textView.text = "Edit 2 : ${v.text}"
                editText3.id -> textView.text = "Edit 3 : ${v.text}"
            }
        return false
        }
    }
}
