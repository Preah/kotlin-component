package io.niceday.button

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.niceday.R
import kotlinx.android.synthetic.main.activity_button.*

class ButtonActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_button)

        button1.setOnClickListener {
            textView.text = "BUTTON1 를 클릭 하셨습니다."
        }

        button2.setOnClickListener {
            textView.text = "BUTTON2 를 클릭 하셨습니다."
        }
    }
}
