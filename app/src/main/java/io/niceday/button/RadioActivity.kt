package io.niceday.button

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import io.niceday.R
import kotlinx.android.synthetic.main.activity_radio.*

class RadioActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio)

        radioButton1.setOnClickListener { view -> onClicked(view) }
        radioButton2.setOnClickListener { view -> onClicked(view) }
        radioButton3.setOnClickListener { view -> onClicked(view) }

        button1.setOnClickListener {

            when (radioGroup.checkedRadioButtonId) {
                R.id.radioButton1 -> textView2.text = "Radio Box 1 선택 하셨습니다."
                R.id.radioButton2 -> textView2.text = "Radio Box 2 선택 하셨습니다."
                R.id.radioButton3 -> textView2.text = "Radio Box 3 선택 하셨습니다."
                else -> textView2.text = ""
            }
        }
    }

    fun onClicked(view: View) {

        if (view is RadioButton) {

            when (view.id) {
                R.id.radioButton1 -> textView1.text = if (view.isChecked) "Radio Box 1 선택 하셨습니다." else ""
                R.id.radioButton2 -> textView1.text = if (view.isChecked) "Radio Box 2 선택 하셨습니다." else ""
                R.id.radioButton3 -> textView1.text = if (view.isChecked) "Radio Box 3 선택 하셨습니다." else ""
                else -> textView1.text = ""
            }
        }
    }
}
