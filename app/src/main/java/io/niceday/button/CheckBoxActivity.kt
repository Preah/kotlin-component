package io.niceday.button

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import androidx.appcompat.app.AppCompatActivity
import io.niceday.R
import kotlinx.android.synthetic.main.activity_check_box.*

class CheckBoxActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_box)

        checkBox1.setOnClickListener { view -> onClicked(view) }
        checkBox2.setOnClickListener { view -> onClicked(view) }
    }

    fun onClicked(view: View) {

        if (view is CheckBox) {

            when (view.id) {
                R.id.checkBox1 -> textView.text = if(view.isChecked) "Check Box 1 선택 하셨습니다." else ""
                R.id.checkBox2 -> textView.text = if(view.isChecked) "Check Box 2 선택 하셨습니다." else ""
                else -> textView.text = ""
            }
        }
    }
}
